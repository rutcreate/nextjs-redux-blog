/* eslint-disable @next/next/no-img-element */
import Link from 'next/link'
import React from 'react'
import { Post } from '../store/posts'

type BlogProps = {
  post: Post
}

const BlogPost: React.FC<BlogProps> = ({ post, children }) => {
  return (
    <div className="shadow-lg rounded-xl overflow-hidden">
      <img src={post.image} alt={post.title} />
      <div className="px-4">
        <Link href={`/blogs/${post.slug}`}>
          <a className="inline-block py-2 text-lg">{post.title}</a>
        </Link>
        {children && <div className="">{children}</div>}
      </div>
    </div>
  )
}

export default BlogPost
