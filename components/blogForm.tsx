import { FormEvent, useState } from 'react'
import { Post } from '../store/posts'

type BlogFormProps = {
  post?: Post
  onSubmit: (post: Post) => void
}

const BlogForm: React.FC<BlogFormProps> = ({ post, onSubmit }) => {
  const [title, setTitle] = useState(post ? post.title : '')
  const [image, setImage] = useState(post ? post.image : '')
  const [body, setBody] = useState(post ? post.body : '')
  const [slug, setSlug] = useState(post ? post.slug : '')

  const onFormSubmit = (e: FormEvent<HTMLFormElement>) => {
    e.preventDefault()
    onSubmit({ title, image, body, slug })
  }

  return (
    <form onSubmit={onFormSubmit}>
      <div className="mb-2">
        <label className="block">Title</label>
        <input
          type="text"
          className="w-full rounded"
          value={title}
          onChange={(e) => setTitle(e.currentTarget.value)}
        />
      </div>
      <div className="mb-2">
        <label className="block">Slug</label>
        <input
          type="text"
          className="w-full rounded"
          value={slug}
          onChange={(e) => setSlug(e.currentTarget.value)}
        />
      </div>
      <div className="mb-2">
        <label className="block">Image</label>
        <input
          type="text"
          className="w-full rounded"
          value={image}
          onChange={(e) => setImage(e.currentTarget.value)}
        />
      </div>
      <div className="mb-2">
        <label className="block">Body</label>
        <textarea
          value={body}
          className="w-full rounded"
          onChange={(e) => setBody(e.currentTarget.value)}
        />
      </div>
      <button
        className="rounded bg-gray-700 text-white px-4 py-2 hover:bg-gray-600"
        type="submit"
      >
        Submit
      </button>
    </form>
  )
}

export default BlogForm
