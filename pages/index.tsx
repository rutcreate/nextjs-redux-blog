import type { NextPage } from 'next'
import BlogPost from '../components/blogPost'
import { useAppSelector } from '../hook'

const Home: NextPage = () => {
  const posts = useAppSelector((state) => state.posts.posts)

  return (
    <div className="">
      <h1 className="text-3xl py-4">Blog Posts</h1>
      <div className="grid grid-cols-4 gap-4">
        {posts.map((post) => (
          <BlogPost key={post.slug} post={post} />
        ))}
      </div>
    </div>
  )
}

export default Home
