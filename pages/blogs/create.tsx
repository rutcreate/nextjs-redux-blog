import type { NextPage } from 'next'
import { useRouter } from 'next/dist/client/router'
import BlogForm from '../../components/blogForm'
import { useAppDispatch } from '../../hook'
import { add, Post } from '../../store/posts'

const CreatePage: NextPage = () => {
  const router = useRouter()
  const dispatch = useAppDispatch()

  const onFormSubmit = (post: Post) => {
    dispatch(add(post))
    router.push('/')
  }

  return (
    <div className="">
      <h1 className="text-3xl py-4">Create</h1>
      <BlogForm onSubmit={onFormSubmit} />
    </div>
  )
}

export default CreatePage
