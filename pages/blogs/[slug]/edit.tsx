import type { NextPage } from 'next'
import { useRouter } from 'next/dist/client/router'
import { useEffect } from 'react'
import BlogForm from '../../../components/blogForm'
import { useAppDispatch, useAppSelector } from '../../../hook'
import { Post, update } from '../../../store/posts'

const EditPostPage: NextPage = () => {
  const router = useRouter()
  const dispatch = useAppDispatch()
  const posts = useAppSelector((state) => state.posts.posts)
  const post = posts.find((post) => post.slug === router.query.slug)

  useEffect(() => {
    if (!post) {
      router.push('/')
      return
    }
  }, [])

  const onFormSubmit = (post: Post) => {
    dispatch(update(post))
    router.push(`/blogs/${post.slug}`)
  }

  return (
    <div className="">
      {post && (
        <div className="">
          <h1 className="text-3xl py-4">Edit {post.title}</h1>
          <BlogForm post={post} onSubmit={onFormSubmit} />
        </div>
      )}
    </div>
  )
}

export default EditPostPage
