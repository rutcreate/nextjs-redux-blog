import type { NextPage } from 'next'
import { useRouter } from 'next/dist/client/router'
import Link from 'next/link'
import { useEffect } from 'react'
import { useAppDispatch, useAppSelector } from '../../../hook'
import { destroy, Post } from '../../../store/posts'

const BlogPostPage: NextPage = () => {
  const router = useRouter()
  const dispatch = useAppDispatch()
  const posts = useAppSelector((state) => state.posts.posts)
  const post = posts.find((post) => post.slug === router.query.slug)

  const deletePost = (post: Post) => {
    dispatch(destroy(post.slug))
    router.push('/')
  }

  useEffect(() => {
    if (!post) {
      router.push('/')
      return
    }
  }, [])

  return (
    <div className="">
      {post && (
        <div className="">
          <img
            src={post.image}
            alt={post.title}
            className="w-full max-w-full"
          />

          <h1 className="text-3xl py-4">{post.title}</h1>

          <div className="py-4">{post.body}</div>

          <div className="flex gap-2 pb-4">
            <Link href={`/blogs/${post.slug}/edit`}>
              <a className="bg-blue-600 text-white rounded px-4 py-2 inline-block">
                Edit
              </a>
            </Link>

            <button
              className="bg-red-500 text-white rounded px-4 py-2 inline-block"
              onClick={() => deletePost(post)}
            >
              Delete
            </button>
          </div>
        </div>
      )}
    </div>
  )
}

export default BlogPostPage
