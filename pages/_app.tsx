import type { AppProps } from 'next/app'
import { useRouter } from 'next/dist/client/router'
import Link from 'next/link'
import { Provider } from 'react-redux'
import store from '../store'
import '../styles/globals.css'

const Navigation = () => {
  const router = useRouter()

  const menuItems = [
    { title: 'Home', href: '/' },
    { title: 'Create Post', href: '/blogs/create' },
  ]

  return (
    <div className="bg-white shadow-md px-4 flex items-center">
      <div className="container mx-auto">
        {menuItems.map((item) => (
          <Link key={item.title} href={item.href}>
            <a
              className={`py-4 px-2 inline-block ${
                router.pathname === item.href ? 'bg-gray-600 text-white' : ''
              }`}
            >
              {item.title}
            </a>
          </Link>
        ))}
      </div>
    </div>
  )
}

function MyApp({ Component, pageProps }: AppProps) {
  return (
    <Provider store={store}>
      <Navigation />

      <div className="container mx-auto">
        <Component {...pageProps} />
      </div>
    </Provider>
  )
}
export default MyApp
